MailmanMQArchiver, a message queues based archiver built on messaging systems

MailmanMQArchiver provides archiving interfaces for GNU Mailman based in
message queues implementation.
MailmanMQArchiver, though currently supporting RabbitMQ for messaging system backend,
can be plugged with other backends.

The project page is http://www.gitlab.com/anirudhdahiya9/MailmanMQArchiver

Why MessageQueueArchiver?
=========================

The older archiving systems in Mailman core are based on the IArchiver interface from where
the cleared mails are passed on to archivers via different archiver specific methods such
as a simple POST request or an email directly to the archive servers .
With many popular api's available for message queue systems, it makes perfect sense to exploit the
flexibility and reliability of these solutions.
RabbitMQMessaging system is a message queue based system pluggable to the Mailman core archiving interface(IArchiver)
which enables reliable, asynchronous and multi-client archiving.
This also leads to a more robust distributed archiving architecture while also expanding the scope of Mailman server
allowing it to hook it up to a wide variety of web applications like static web page generators, event trackers
or websockets servers.

This package provides two interfaces, based on popular message patterns:
*MQArchiver
*PubMQArchiver

Contents
========
*Interfaces
*Installation
*Archiver Configuration
*Backends
