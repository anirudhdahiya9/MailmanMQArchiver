import mock
import MailmanMQArchiver
from unittest import TestCase


class FakeDomain:
    """Fake a Mailman domain implementing the IDomain interface"""
    def __init__(self, domain):
        self.mail_host = domain
        self.base_url = "http://" + domain


class FakeList:
    """Fake a Mailman list implementing the IMailingList interface"""
    def __init__(self, name):
        self.fqdn_listname = name
        self.domain = FakeDomain("lists.example.com")


class FakeMessage:
    """Fake a Mailman Message"""
    def __init__(self):
        self.body = 'Hi! This is a test message'

    def as_string(self):
        return self.body

class TestBackend:
    """Fake Backend For testing"""
    def __init__(self):
        return

    def connect(self):
        """ Connection Establishment calls"""
        raise NotImplementedError

    def close_connection(self):
        """ Connection Closure Calls """
        raise NotImplementedError

    def get_config_mq(self, archiver_configuration=False):
        """ Set Parameters for MQ Interface from conf file or set to defaults"""
        raise NotImplementedError

    def get_config_pubsub(self, archiver_configuration=False):
        """ Set Parameters for PubSub Interface  from conf file or set to default """
        raise NotImplementedError

    def archive_message_mq(self, mlist, msg):
        """ Message archive calls for MQ interface """
        raise NotImplementedError

    def archive_message_pubsub(self, mlist, msg):
        """ Message archive calls for PubSub interface """
        raise NotImplementedError



class TestMQArchiver(TestCase):

    def setUp(self):
        self.archiver = MailmanMQArchiver.MQArchiver()
        self.archiver.name = 'sample_archive'
        self.archiver.backend = TestBackend()
        self.mlist = FakeList("list@lists.example.com")
        self.msg = FakeMessage()

    def tearDown(self):
        pass

    @mock.patch('__main__.TestBackend.connect', autospec=True)
    def test_setup_connection(self, mock_backend):
        self.archiver.setup_connection()
        mock_backend.assert_called_with()

    @mock.patch('__main__.TestBackend.close_connection', autospec=True)
    def test_close_connection(self, mock_backend):
        self.archiver.close_connection()
        mock_backend.assert_called_with()

    @mock.patch('MailmanMQArchiver.mqarchiver.MQArchiver.close_connection', autospec=True)
    @mock.patch('MailmanMQArchiver.mqarchiver.MQArchiver.backend.archive_message_mq', autospec=True)
    @mock.patch('MailmanMQArchiver.mqarchiver.MQArchiver._get_configuration',
                autospec=True)
    @mock.patch('MailmanMQArchiver.mqarchiver.MQArchiver.setup_connection', autospec=True)
    def test_archive_message(self, mock_setup_connection, mock_config, mock_backend_archive, mock_close_connection):
        self.archiver.name = 'coolarchiver'
        self.archiver.archive_message(self.mlist, self.msg)
        mock_config.assert_called_once_with(self.archiver)
        mock_setup_connection.assert_called_once_with()
        mock_backend_archive.assert_called_once_with(self.mlist,self.msg)
        mock_close_connection.assert_called_once_with()