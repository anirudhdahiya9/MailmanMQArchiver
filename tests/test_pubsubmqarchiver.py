import mock
import MailmanMQArchiver
from unittest import TestCase


class FakeDomain:
    """Fake a Mailman domain implementing the IDomain interface"""
    def __init__(self, domain):
        self.mail_host = domain
        self.base_url = "http://" + domain


class FakeList:
    """Fake a Mailman list implementing the IMailingList interface"""
    def __init__(self, name):
        self.fqdn_listname = name
        self.domain = FakeDomain("lists.example.com")


class FakeMessage:
    """Fake a Mailman Message"""
    def __init__(self):
        self.body = 'Hi! This is a test message'

    def as_string(self):
        return self.body


class TestMQArchiver(TestCase):

    def setUp(self):
        self.archiver = MailmanMQArchiver.PubMQArchiver()
        self.archiver.name = 'sample_archive'
        self.mlist = FakeList("list@lists.example.com")
        self.msg = FakeMessage()

    def tearDown(self):
        pass

    @mock.patch('MailmanMQArchiver.pubsubmqarchiver.pika', autospec=True)
    def test_setup_connection(self, mock_pika):
        self.archiver.rbmq_server_host = 'localhost'
        self.archiver.setup_connection()
        mock_pika.ConnectionParameters.assert_called_once_with(
                host='localhost')
        mock_pika.BlockingConnection.assert_called_once_with(
                mock_pika.ConnectionParameters(host='localhost'))
        conn_para = mock_pika.ConnectionParameters(host='localhost')
        mock_conn = mock_pika.BlockingConnection(conn_para)
        mock_conn.channel.assert_called_once_with()
        mock_channel = mock_conn.channel()
        mock_channel.exchange_declare.assert_called_once_with(
            exchange='pubsub', type='fanout')

    @mock.patch(
        'MailmanMQArchiver.pubsubmqarchiver.PubMQArchiver._get_configuration',
        autospec=True)
    @mock.patch('MailmanMQArchiver.pubsubmqarchiver.pika', autospec=True)
    def test_archive_message(self, mock_pika, mock_config):
        self.archiver.name = 'coolarchiver'
        self.archiver.archive_message(self.mlist, self.msg)
        mock_config.assert_called_once_with(self.archiver)
        mock_con_para = mock_pika.ConnectionParameters(host='localhost')
        mock_channel = mock_pika.BlockingConnection(mock_con_para).channel()
        mock_channel.basic_publish.assert_called_once_with(
            exchange='pubsub', routing_key='', body=self.msg.as_string())
