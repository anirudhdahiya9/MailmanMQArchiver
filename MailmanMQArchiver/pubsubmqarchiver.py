import logging
from mailman.config import config
from mailman.config.config import external_configuration as ext_cfg
from mailman.config.config import MissingConfigurationFileError
from mailman.interfaces.archiver import IArchiver
from MailmanMQArchiver import backends
from zope.interface import implementer

logger = logging.getLogger("mailman.archiver")


@implementer(IArchiver)
class PubMQArchiver:
    """Message Queue Based Archiver in RabbitMQ"""

    name = "PubMQArchiver"
    is_enabled = False

    def __init__(self):
        self.connection = None
        self.channel = None
        self.exchange = None
        self.archiver_configuration = None
        self.rbmq_server_host = None
        self.backend = None
        self.name = None

    def _get_configuration(self):
        # try to fetch backend
        config_section = getattr(config.archiver, self.name)
        config_path = config_section.configuration
        try:
            self.archiver_configuration = ext_cfg(config_path)
        except MissingConfigurationFileError:
            # No config file, default to RabbitMQBackend
            self.backend = backends.RabbitMQBackend()
            self.backend.get_config_pubsub()
            return
        # Config file fetched, now fetch backend name
        try:
            backend_name = self.archiver_configuration.get('general', 'backend')
            backend_class_ = getattr(backends, backend_name)
            self.backend = backend_class_()
        except AttributeError:
            # Backend not mentioned, default to RabbitMQBackend
            self.backend = backends.RabbitMQBackend()
        # Backend set, now fetch attributes host address and exchange name
        self.backend.get_config_pubsub(self.archiver_configuration)

    # Common Channel for multiple archives, thus no list_url and permalink
    def list_url(self, mlist):
        return None

    def permalink(self, mlist, msg):
        return None

    def setup_connection(self):
        """For future wrapper functions while connecting/disconnecting"""
        self.backend.connect()

    def close_connection(self):
        self.backend.close_connection()

    def archive_message(self, mlist, msg):
        """Send message to the archiver"""
        self._get_configuration()
        self.setup_connection()
        self.backend.archive_message_pubsub(mlist, msg)
        self.close_connection()
