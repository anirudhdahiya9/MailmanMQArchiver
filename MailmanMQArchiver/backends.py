import logging
import pika
from pika.exceptions import RecursionError, NoFreeChannels, AMQPChannelError
from configparser import NoOptionError, NoSectionError

log = logging.getLogger("mailman.archiver")


# The calls to the backend is made in the following sequence
# get_conf, connect, archive_message, close_connection
class BaseClass:
    """Base Class for Backend Classes"""
    def __init__(self):
        raise NotImplementedError

    def connect(self):
        """ Connection Establishment calls"""
        raise NotImplementedError

    def close_connection(self):
        """ Connection Closure Calls """
        raise NotImplementedError

    def get_config_mq(self, archiver_configuration=False):
        """ Set Parameters for MQ Interface from conf file or set to defaults"""
        raise NotImplementedError

    def get_config_pubsub(self, archiver_configuration=False):
        """ Set Parameters for PubSub Interface  from conf file or set to default """
        raise NotImplementedError

    def archive_message_mq(self, mlist, msg):
        """ Message archive calls for MQ interface """
        raise NotImplementedError

    def archive_message_pubsub(self, mlist, msg):
        """ Message archive calls for PubSub interface """
        raise NotImplementedError


class RabbitMQBackend(BaseClass):
    # API Calls for RabbitMQ backend
    def __init__(self):
        self.host = None
        self.connection = None
        self.channel = None
        self.exchange_name = None

    def connect(self):
        try:
            conn_para = pika.ConnectionParameters(host=self.host)
            self.connection = pika.BlockingConnection(conn_para)
            self.channel = self.connection.channel()
        except (RecursionError, NoFreeChannels, AMQPChannelError) as e:
            log.error("Connection issue with RabbitMQ Server: {}".format(e))

    # Sets the host address
    def get_config_mq(self, archiver_configuration=None):
        if archiver_configuration is not None:
            try:
                self.host = archiver_configuration.get(
                    'general',
                    'host')
            except (NoSectionError, NoOptionError):
                # default to localhost
                log.error('RabbitMQBackend : Host not specified, defaulting\
                 to localhost')
                self.host = 'localhost'
        else:
            self.host = 'localhost'

    # pubsub also needs an exchange name
    def get_config_pubsub(self, archiver_configuration=None):
        # Fetch host address
        self.get_config_mq(archiver_configuration)
        # Fetch Exchange Name
        if archiver_configuration is not None:
            try:
                self.exchange_name = archiver_configuration.get('general',
                                                                'exchange_name')
            except (NoSectionError, NoOptionError):
                log.error("RabbitMQBackend : exchange_name not specified, \
                defaulting to pubsub")
                self.exchange_name = 'pubsub'
        # default name for the common exchange, can be changed in config file
        else:
            self.exchange_name = 'pubsub'

    def archive_message_mq(self, mlist, msg, archive_name):
        """Archive message calls for message queue implementation"""
        result = self.channel.queue_declare(queue=archive_name)
        queue_name = result.method.queue
        self.channel.basic_publish(exchange='',
                                   routing_key=queue_name,
                                   body=msg.as_string())

    def archive_message_pubsub(self, mlist, msg):
        """Archive message calls for publisher subscriber pattern implementation"""
        self.exchange = self.channel.exchange_declare(
            exchange=self.exchange_name,
            type='fanout')
        self.channel.basic_publish(exchange=self.exchange_name,
                                   routing_key='',
                                   body=msg.as_string())

    def close_connection(self):
        self.connection.close()
