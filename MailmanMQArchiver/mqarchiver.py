import logging
from mailman.config import config
from mailman.config.config import external_configuration as ext_cfg
from mailman.config.config import MissingConfigurationFileError
from mailman.interfaces.archiver import IArchiver, ArchivePolicy
from mailman.utilities.string import expand
from MailmanMQArchiver import backends
from pika.exceptions import RecursionError, NoFreeChannels, AMQPChannelError
from zope.interface import implementer
from configparser import NoOptionError, NoSectionError

log = logging.getLogger("mailman.archiver")


@implementer(IArchiver)
class MQArchiver:
    """Message Queue Based Archiver in RabbitMQ"""
    def __init__(self):
        self.connection = None
        self.channel = None
        self.archiver_configuration = None
        self.rbmq_server_host = None
        self.base_url = None
        self.permalink_url = None
        self.name = None
        self.backend = None

    # configuration is loaded explicitly on interface function calls,
    # and not in __init__ because the self.name is reset after instatiation
    # due to design constraints(See ListArchiverSet and config.archivers)
    def _get_configuration(self, backend_required=True):
        # try to fetch configuration file path
        config_section = getattr(config.archiver, self.name)
        config_path = config_section.configuration
        # try to load configuration file, and default in case of failure
        try:
            self.archiver_configuration = ext_cfg(config_path)
        except MissingConfigurationFileError:
            # Config file not loaded
            log.error("The configuration file at {0} could not be loaded,\
             please check the path, defaulting to RabbitMQ backend".format(config_path))
            self.backend = backends.RabbitMQBackend()
            self.backend.get_config_mq()
            self.base_url = None
            self.permalink_url = None
            return
        # From now on, config file loaded
        # try to get base_url and permalink_url, otherwise restore to defaults
        try:
            self.base_url = self.archiver_configuration.get(
                'archiver.' + self.name, 'base_url')
        except (NoSectionError, NoOptionError):
            self.base_url = None
        try:
            self.permlink_url = self.archiver_configuration.get(
                'archiver.' + self.name, 'permalink_url')
        except (NoSectionError, NoOptionError):
            self.permalink_url = None
        # Let backend read configuration if required
        if backend_required:
            # Check if backend specified in config file
            try:
                backend_name = self.archiver_configuration.get('general',
                                                               'backend')
                backend_class_ = getattr(backends, backend_name)
                self.backend = backend_class_()
            except AttributeError:
                # Backend not specified, default to RabbitMQBackend
                log.error("Backend {0} specified in archiver config file \
                `   could not be found in backends".format(backend_name))
                self.backend = backends.RabbitMQBackend()
            # Let backend read configuration
            self.backend.get_config_mq(self.archiver_configuration)

    def list_url(self, mlist):
        self._get_configuration(backend_required=False)
        if mlist.archive_policy is ArchivePolicy.public:
            if self.base_url is not None:
                return expand(self.base_url,
                              dict(listname=mlist.fqdn_listname,
                                   hostname=mlist.domain.url_host,
                                   fqdn_listname=mlist.fqdn_listname,
                                   ))
        return None

    def permalink(self, mlist, msg):
        self._get_configuration(backend_required=False)
        if mlist.archive_policy is not ArchivePolicy.public:
            return None
        # It is the LMTP server's responsibility to ensure that the message has
        # a Message-ID-Hash header.  For backward compatibility, fallback to
        # searching for X-Message-ID-Hash.  If the message has neither, then
        # there's no permalink.
        message_id_hash = msg.get('message-id-hash')
        if message_id_hash is None:
            message_id_hash = msg.get('x-message-id-hash')
        if message_id_hash is None:
            return None
        if isinstance(message_id_hash, bytes):
            message_id_hash = message_id_hash.decode('ascii')
        if self.permalink_url is not None:
            return expand(self.permalink_url,
                          dict(
                              id_hash=message_id_hash,
                              listname=mlist.fqdn_listname,
                              hostname=mlist.domain.url_host,
                              fqdn_listname=mlist.fqdn_listname)
                          )
        return None

    def setup_connection(self):
        """For future wrapper functions while connecting/disconnecting"""
        self.backend.connect()

    def close_connection(self):
        self.backend.close_connection()

    def archive_message(self, mlist, msg):
        """Send message to the archiver, See IArchiver"""
        self._get_configuration(backend_required=True)
        self.setup_connection()
        # The queue name is set as self.name, which is the
        # archiver section header in the mailman.cfg file
        self.backend.archive_message_mq(mlist, msg, self.name)
        self.close_connection()
