The package provides two interfaces based on different message queue patterns :
*MQArchiver
*PubMQArchiver

These interfaces can be configured to use the backend of choice, from the list of
available ones by mentioning the name in backend parameter in the archiver configuration
file. See Archiver Configuration for more details.
Currently, it supports RabbitMQ. See Backends page for more backend specific information.

MQArchiver
==========
The interface MQArchiver is a simple message queue based archiver, with a separate queue for
each application(eg archiver, static web page generator etc).
This interface is designed to be provide closed queues for reliable transfer of mails between
the Mailman core server and the application.
Note that the queue name to be connected with is the same as the section header of the section
which registers the archive/appllication in the mailman.cfg configuration.
Also, the permalink and base_url strings can be provided in the archiver configuration file.
See Archiver Configuration for more details.
Package class : MailmanMQArchiver.MQArchiver

PubMQArchiver
=============
This interface provides an open access point to which applications can connect to and receive feed.
This message pattern is based on the Publisher-Subscriber model, thus The interface does not need any
 information about subscribers, and simply sends out the feed to a common open access point.
Package class : MailmanMQArchiver.PubMQArchiver