This page discusses different supported backends.

RabbitMQ
========
RabbitMQ is a popular open source messaging system and is the default set backend for MailmanMQArchiver.
It provides both message queue based archiving and publisher-subscriber pattern based archiving.

Message Queues based Archiving
==============================
This is based on message queues based messaging pattern, and exploits individual queues for archives
to provide closed and reliable messaging which can survive client restarts. This is implemented with
the MQArchiver interface. Note that the queue name is set as the name of the archive mentioned in the
mailman configuration file while registering the archive.
For example,
    [archiver.archive_name]

    In this case, the queue name is set to the archive_name.

Publisher-Subscriber based Archiving
====================================
This is based on the publisher subscriber model. Here all the mails are forwarded to the common acces
point called an exchange. Multiple applications can connect their queues to this exchange and receive
asynchronous feed.
Here, the clients(who receive feed) need not be known by the server, thus providing an open access point.
Though the default name for the exchange is set to
'pubsub', it can be changed in the archiver configuration file. See archiver Configuration for more
details.

Below, are some parameters that should be configured in the archiver configuration file.
In case, of missing configuration file, or missing configuration definition the values are
set to the default values.

RabbitMQServerHost
==================
This is the address of the RabbitMQ server address where the messages are sent to by the interface.
The queues and the exchanges are located at this server.
Though the default rabbitmq server address is set to 'localhost', it can be configured like this :

    [general]
    backend: RabbitMQ
    host : rabbitmq_server_address

Exchange Name
=============
This field is relevant to only PubMQArchiver interface.
The exchange name (i.e. the connecting point in the RabbitMQ server) for applications is
set to 'pubsub' by default. This can be changed by mentioning exchange_name field under general
headers in the archiver configuration file.
For Example,

    [general]
    backend: RabbitMQ
    host: rabbitmq_server_address
    exchange_name: abcxyz
