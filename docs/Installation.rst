INSTALLATION
============
The project is located at `http://gitlab.com/anirudhdahiya9/RabbitMQMessageQueueArchiver`

In your mailman env, install this package using the command: python setup.py install

DEVELOPMENT
===========
In your mailman env, install this package using python setup.py develop

MAILMAN CONFIGURATION
=====================
In your mailman configuration file(mailman.cfg), you can register this archiver interface
to use with an archiver by adding the following lines(see mailman schema.cfg for more examples) :

    [archiver.archive_name]
    class : python_path_to_RabbitMQArchiver_interface
    enable : yes
    configuration : python_path_to_archiver_configuration_file

The python path to the interface class should be like:
* For Simple Message Queue interface  : python:MailmanMQArchiver.MQArchiver
* For Publisher-Subsciber based interface : python:MailmanMQArchiver.PubMQArchiver

It is highly recommended to include a configuration file for the archiver, for more flexibility. The details
about setting up a configuration can be found in Archiver Configuration Documentation.
A sample configuration file mqarchiver.cfg.sample file is provided along the package.
