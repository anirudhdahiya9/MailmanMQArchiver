ARCHIVER CONFIGURATION
======================
The configuration file for the archiver here is supposed to serve as the place to store
configuration details for the interface and archives, as discussed below

BACKEND
=======
Admins can choose to use a specific backend in a configuration.
To do so, one needs to mention the backend class name(available from the Backends page)
along the parameter backend under the general section. The default backend is currently
set as RabbitMQ, and in case of missing configuation file, or parameter definition in the
archiver configuration file, it falls back to this default value.

Example,

    [general]
    backend: RabbitMQ   #class name of the backend

Base URL and Permalink
======================
This field is relevant to only MQArchiver interface.
The  base_url and permalink url strings are by default set to None.
Example for adding base_url and permalink information to the configuration file.

    [archiver.archive_name]
    base_url: https://$hostname/archives/$listname
    permalink_url : https://$hostname/archives/$listname/$id_hash

Backend Specific Configuration
==============================
There may be some parameter specific parameters, which need to be defined.
Refer to the Backends page for more backend specific configuration.

IMPORTANT:
==========
The archive_name you set in the category section above and in mailman.cfg should be the same,
and is set as the queue name in the rabbitmq server, and thus all the mails are sent to the
rabbitmq server at the queue with that name.

 For example, if you register your archive in mailman.cfg like,
    [archiver.archive_xyz]
    class : python_path_to_RabbitMQArchiver
    enable : yes (if you want to enable archiver)
    configuration : path_to_your_configuration_file

    then, the mails to this archive will be sent to the queue 'archive_xyz', and shall be
    retrieved from this queue itself.

It is recommended not to changee the archive_name once set in configuration, i.e. the archive name in the archiver headers
in the mailman.cfg file and archiver configuration file. This causes the old archiver entry in the database to be left unable
to access any archiver interface, while creating a new entry in the database with the new archive_name.
Although this doesn't halt archiving, but now the mails are now sent to the queue with the new name.
This requires for change in the receiver side of the archive appliaction to connect to the queue with the "new" name.
Thus requiring changes in the receiver side of the archive application.
An error is logged in mailman logs about such "stale" archivers, and system admins are requested to delete
them.
Thus in case renaming is required, please note the implied change in the queue name.