from setuptools import setup, find_packages

setup(
    name="MailmanMQArchiver",
    version="0.1",
    description = "Message Queues based archiving interface for GNU Mailman",
    maintainer = "Anirudh Dahiya",
    license = 'GPLv3',
    keywords = "email archiving archive queue publisher subscriber",
    url = "https://gitlab.com/anirudhdahiya9/MailmanMQArchiver",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'setuptools',
        'mailman',
        'requests',
        'zope.interface',
        'pika',
    ],
)
